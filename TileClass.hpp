#ifndef Tile_H
#define Tile_H
#include <vector>
#include "ItemClass.hpp"
class Tile {

private:
  char graphic;
  bool collision, canSee = false, hasSeen = false;
  int seenColor, unseenColor, xpos, ypos;
  vector<Item> itemStack;

public:
  bool operator==(const Tile& leftoperand) {
    if (leftoperand.xpos == this->xpos && leftoperand.ypos == this->ypos)
      return true;
    return false;
  }

  Tile() {}
  Tile(char graphic, bool collision, int seenColor, int unseenColor, int xpos, int ypos)
    :graphic(graphic),
    collision(collision),
    seenColor(seenColor),
    unseenColor(unseenColor),
    xpos(xpos),
    ypos(ypos) {
  };
  //Tile(const Tile& copied)
  //	: Tile(copied.graphic, copied.collision, copied.seenColor, copied.unseenColor, copied.xpos, copied.ypos){};

  void addItemToStack(Item item) {
    itemStack.push_back(item);
  };

  bool getVisualHistory() { return hasSeen; }
  bool getVisualStatus() { return canSee; }
  bool getCollision() { return collision; }
  int getXpos() { return xpos; }
  int getYpos() { return ypos; }
  char getGraphic() { return graphic; }

  char getDrawnGraphic() {
    if (itemStack.size() == 0)
      return graphic;
    else
      return itemStack[itemStack.size() - 1].getGraphic();
  }

  int getColor() {
    if (canSee)
      return seenColor;
    else
      return unseenColor;
  }

  void setVisualStatus(bool sight) {
    canSee = sight;
    if (canSee == true && hasSeen == false)
      hasSeen = true;
  }
};
#endif