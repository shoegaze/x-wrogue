#ifndef Entity_H
#define Entity_H

#include "ItemClass.hpp"
#include "Items.hpp"
#include <iostream>
using namespace std;

class Player {

private:

  char graphic;

  int xp = 0, nxp = 1000, currenthealth = 40, maxhealth = 40, currentmana = 20, maxmana = 20,
    strength = 10, intelligence = 6, agility = 8, xpos, ypos, level = 1;
  const char* name;
  ArmorItem Head = bareHead, Torso = bareChest, Arms = bareArms, Hands = bareHands, Legs = bareLegs, Feet = bareFeet;
  Weapon LeftHeldItem = emptyHand, RightHeldItem = emptyHand /*EquippedAmmo*/;
  vector<Item> inventory;

public:

  Player(char graphic, int xpos, int ypos, const char* name)
    :graphic(graphic),
    xpos(xpos),
    ypos(ypos),
    name(name) {
  }

  char getGraphic() { return graphic; }

  int getYpos() { return ypos; }
  void setYpos(int param) { ypos = param; }

  int getXpos() { return xpos; }
  void setXpos(int param) { xpos = param; }

  int getCurrentHealth() { return currenthealth; }
  void setCurrentHealth(int param) { currenthealth = param; }

  int getMaxHealth() { return maxhealth; }
  void setMaxHealth(int param) { maxhealth = param; }

  int getMaxMana() { return maxmana; }
  void setMaxMana(int param) { maxmana = param; }

  int getCurrentMana() { return currentmana; }
  void setCurrentMana(int param) { currentmana = param; }

  int getStrength() { return strength; }
  void setStrength(int param) { strength = param; }

  int getIntelligence() { return intelligence; }
  void setIntelligence(int param) { intelligence = param; }

  int getAgility() { return agility; }
  void setAgility(int param) { agility = param; }

  int getXP() { return xp; }
  void setXP(int param) { xp = param; }

  int getNXP() { return nxp; }
  void setNXP(int param) { nxp = param; }

  int getLevel() { return level; }
  void setLevel(int param) { level = param; }

  const char* getName() { return name; }

  /*HeldItem getRightHeldItem(){
    return RightHeldItem;
  }*/
};


#endif