	//ArmorItem item(armor, grade, slot, name, description, graphic)
	ArmorItem	bareHead(0, 0, "Head", "Head", "It's your head. There's nothing on it.", 'h');
	ArmorItem	bareChest(0, 0, "Torso", "Chest", "It's your torso. You're not wearing anything there.", 't');
	ArmorItem	bareLegs(0, 0, "Legs", "Loincloth", "You've got something on, at least.", 'l');
	ArmorItem	bareFeet(0, 0, "Feet", "Feet", "They're your feet. You've even kept all 10 toes, thus far.", 'f');
	ArmorItem	bareArms(0, 0, "Arms", "Arms", "They're your arms. Got some hair on them.", 'a');
	ArmorItem	bareHands(0, 0, "Hands", "Hands", "They're your hands. Still got all the digits, too.", 'g');
	//Weapon(int diceNumber, int diceSize, int armorPen, int grade, int critChance, int accuracy, const char* type,
			//const char* name, const char* description, char graphic)
	Weapon Axe_of_Debugging(10, 10, 10, 100, 50, 100, "Axe", "Axe of Debugging", "Axe of the gods", 'a');
	Weapon emptyHand(1, 4, 0, 0, 5, 80, "Generic", "Empty Hand", "You're not wielding anything in this hand!", 'w');
