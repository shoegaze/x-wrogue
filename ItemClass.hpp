#ifndef Item_H
#define Item_H

class Item{
private:
	char graphic;
	const char* name;
	const char* description;

public:
	Item(const char* name, const char* description, char graphic)
		:name(name),
		description(description),
		graphic(graphic)
	{}

	char getGraphic(){return graphic;}
	const char* getName(){return name;}
	const char* getDescription(){return description;}
};
		class ArmorItem: public Item{
		private:
			int armor, grade;
			const char* slot;		

		public:
			ArmorItem(int armor, int grade, const char* slot, const char* name, const char* description, char graphic)
				:armor(armor),
				grade(grade),
				slot(slot),
				Item(name, description, graphic)
			{}

			int getArmor(){return armor;}
			int getGrade(){return grade;}
			const char* getSlot(){return slot;}
		};


			class Potion: public Item{
			public:
				int effect;
				Potion(int effect, const char* name, const char* description, char graphic);
			};

			/*class Ammunition: public Consumable{
			public:
				int effect, stackSize;
				Ammunition(int effect, int stackSize, const char* name, const char* description, char graphic){
					this->effect = effect;
					this->stackSize = stackSize;
					this->name = name;
					this->description = description;
					this->graphic = graphic;
				}
			}noAmmo(0, 0, "No Ammo", "You have no ammunition equipped!", 'p');*/	

/*class MissileWeapon: public HeldItem{
public:
int accuracy, damage, grade;
Ammunition projectile;
MissileWeapon(int accuracy, int damage, int grade, Ammunition projectile,
std::string name, std::string description, char graphic){
this->accuracy = accuracy;
this->damage = damage;
this->grade = grade;
this->projectile = projectile;
this->name = name;
this->description = description;
this->graphic = graphic;
}
};*/

		class Weapon: public Item{
		private:
			int diceNumber, diceSize, armorPen, critChance, accuracy, grade;
			const char* type;	

		public:
			Weapon(int diceNumber, int diceSize, int armorPen, int grade, int critChance, int accuracy, const char* type,
				const char* name, const char* description, char graphic)
				:diceNumber(diceNumber),
				diceSize(diceSize),
				armorPen(armorPen),
				grade(grade),
				critChance(critChance),
				accuracy(accuracy),
				type(type),
				Item(name, description, graphic)
			{}	

			int getDiceNumber(){return diceNumber;}
			int getDiceSize(){return diceSize;}
			int getArmorPen(){return armorPen;}
			int getcritChance(){return critChance;}
		};

		class Shield: public Item{
			private:
				int armor, bashChance, grade;
				const char* type;

			public:
				Shield(int armor, int bashChance, int grade, const char* type, const char* name, const char* description, char graphic)
					:armor(armor),
					bashChance(bashChance),
					grade(grade),
					type(type),
					Item(name, description, graphic)
				{}
			};

			/*class MissileWeapon: public HeldItem{
			public:
				int accuracy, damage, grade; 
				Ammunition projectile;
				MissileWeapon(int accuracy, int damage, int grade, Ammunition projectile, 
					const char* name, const char* description, char graphic){
					this->accuracy = accuracy;
					this->damage = damage;
					this->grade = grade;
					this->projectile = projectile;
					this->name = name;
					this->description = description;
					this->graphic = graphic;
				}
	 		};*/

#endif