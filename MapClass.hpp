#ifndef Map_H
#define Map_H
#include "TileClass.hpp"
#include "ItemClass.hpp"
#include <time.h>
#include <random>
#include <cstdlib>
#include <algorithm>

class Map {

private:

  int mapXmax, mapYmax;

  Tile tileGrid[55][24]; //THE GRID

  vector<Tile> noCollisionTiles;
  vector<Tile> collisionTiles;
  vector<Tile> pathableTiles;
  //vector<Enemy> activeEnemies;

public:

  Map(int mapXmax, int mapYmax)
    :mapXmax(mapXmax),
    mapYmax(mapYmax) {
  }

  int getXmax() { return mapXmax; }
  int getYmax() { return mapYmax; }
  Tile& getTile(int mapXpos, int mapYpos) { return tileGrid[mapXpos][mapYpos]; }

  bool collision(int NewX, int NewY) {		//NewX and NewY are the coordinates of the desired movement location
    if (tileGrid[NewX][NewY].getCollision())
      return true;
    else
      return false;
  }

  Tile chooseTile(int x, int y) {
    char graphic;
    switch (rand() % 2) {
    case 0:
      graphic = '.';
      break;
    case 1:
      graphic = ',';
      break;
    }
    return Tile(graphic, false, 6, 7, x, y);
  }

  void findPathableSpace(int x, int y) {	//Floodfill method
    if (x - 1 >= 0) {
      Tile tile = this->tileGrid[x - 1][y];
      if (!tile.getCollision() && std::find(pathableTiles.begin(), pathableTiles.end(), tile) == pathableTiles.end()) {
        pathableTiles.push_back(tile);
        findPathableSpace(tile.getXpos(), tile.getYpos());
      }
    }
    if (y - 1 >= 0) {
      Tile tile = this->tileGrid[x][y - 1];
      if (!tile.getCollision() && std::find(pathableTiles.begin(), pathableTiles.end(), tile) == pathableTiles.end()) {
        pathableTiles.push_back(tile);
        findPathableSpace(tile.getXpos(), tile.getYpos());
      }
    }

    if (x + 1 < mapXmax) {
      Tile tile = this->tileGrid[x + 1][y];
      if (!tile.getCollision() && std::find(pathableTiles.begin(), pathableTiles.end(), tile) == pathableTiles.end()) {
        pathableTiles.push_back(tile);
        findPathableSpace(tile.getXpos(), tile.getYpos());

      }
    }

    if (y + 1 < mapYmax) {
      Tile tile = this->tileGrid[x][y + 1];
      if (!tile.getCollision() && std::find(pathableTiles.begin(), pathableTiles.end(), tile) == pathableTiles.end()) {
        pathableTiles.push_back(tile);
        findPathableSpace(tile.getXpos(), tile.getYpos());
      }
    }
  }

  void generate(int playerx, int playery) {
    float startX, startY, iteration, dx, dy, width, height;
    int version, wallx, wally, vectorposition;

    dx = (rand() % 4);
    dy = (rand() % 4);
    width = rand() % 4 + 1;
    height = rand() % 3 + 1;
    version = rand() % 4;
    iteration = 0;

    auto eraseFromVector = collisionTiles.begin();
    //std::cout<<"Entering map reset loop\n";
    for (int x = 0; x < mapXmax; x++) {
      for (int y = 0; y < mapYmax; y++) {
        tileGrid[x][y] = Tile(' ', false, 1, 2, x, y);
      }
    }

    //std::cout<<"Erasing pathable tiles\n";
    pathableTiles.clear();
    //std::cout<<"Erasing collision tiles\n";
    collisionTiles.clear();
    //std::cout<<"Erasing noCollisionTiles\n";
    noCollisionTiles.clear();
    //std::cout<<"Emplacing open space, filling vector\n";
    for (iteration = 0; iteration <= 3; iteration++) {
      startX = rand() % 55;
      startY = rand() % 24;
      while (startX >= 0 && startX < mapXmax && startY >= 0 && startY < mapYmax) {
        switch (rand() % 5) {
        case 0:
          dx = (rand() % 20) / 10.0;
          break;
        case 1:
          dy = (rand() % 20) / 10.0;
          break;
        case 2:
          width = rand() % 4 + 1;
          break;
        case 3:
          height = rand() % 2 + 1;
          break;
        case 4:
          version = rand() % 4;
        }
        for (int x = startX - width; x <= startX + width; x++) {
          for (int y = startY - height; y <= startY + height; y++) {
            if (x >= 0 && y >= 0 && x < mapXmax && y < mapYmax) {
              Tile newTile = chooseTile(x, y);
              tileGrid[x][y] = newTile;
              noCollisionTiles.push_back(newTile);
            }
          }
        }
        switch (version) {
        case 0:
          startX += dx;
          break;
        case 1:
          startX -= dx;
          break;
        case 2:
          startY += dy;
          break;
        case 3:
          startY -= dy;
          break;
        }
      }
    }
    //std::cout<<"Emplacing collision tiles, filling vector\n";
    for (int xPos = 0; xPos < mapXmax; xPos++) {
      for (int yPos = 0; yPos < mapYmax; yPos++) {
        Tile tile;
        tile = tileGrid[xPos][yPos];
        if (find(noCollisionTiles.begin(), noCollisionTiles.end(), tile) == noCollisionTiles.end()) {
          tile = Tile('#', true, 1, 2, xPos, yPos);
          tileGrid[xPos][yPos] = tile;
          collisionTiles.push_back(tile);
        }
      }
    }

    //std::cout<<"Placing hallways, erasing corresponding walls from vector\n";
    for (int i = 0; i <= 15; i++) {
      if (collisionTiles.size() > 0) {
        int num = rand() % collisionTiles.size();
        Tile wall = collisionTiles[num];
        wallx = wall.getXpos();
        wally = wall.getYpos();

        for (int x = wallx - rand() % 7; x <= wallx + rand() % 7; x++) {
          if (x > 0 && x < mapXmax) {
            eraseFromVector = find(collisionTiles.begin(), collisionTiles.end(), Tile('#', true, 1, 2, x, wally));

            if (eraseFromVector != collisionTiles.end()) {
              collisionTiles.erase(eraseFromVector);
              tileGrid[x][wally] = chooseTile(x, wally);
            }
          }
        }

        for (int y = wally - rand() % 7; y <= wally + rand() % 7; y++) {
          if (y > 0 && y < mapYmax) {
            eraseFromVector = find(collisionTiles.begin(), collisionTiles.end(), Tile('#', true, 1, 2, wallx, y));

            if (eraseFromVector != collisionTiles.end()) {
              collisionTiles.erase(eraseFromVector);
              tileGrid[wallx][y] = chooseTile(wallx, y);
            }
          }
        }

      }
    }

    //std::cout<<"Ensuring player can spawn\n";
    Tile tile = tileGrid[playerx][playery];
    if (tile.getCollision()) {
      for (int x = playerx - 5; x <= playerx + 5; x++) {
        if (x > 0) {
          eraseFromVector = find(collisionTiles.begin(), collisionTiles.end(), Tile('#', true, 1, 2, x, playery));
          if (eraseFromVector != collisionTiles.end()) {
            collisionTiles.erase(eraseFromVector);
            tileGrid[x][playery] = chooseTile(x, playery);
          }
        }
      }
      for (int y = playery - 5; y <= playery + 5; y++) {
        if (y > 0) {
          eraseFromVector = find(collisionTiles.begin(), collisionTiles.end(), Tile('#', true, 1, 2, playerx, y));
          if (eraseFromVector != collisionTiles.end()) {
            collisionTiles.erase(eraseFromVector);
            tileGrid[playerx][y] = chooseTile(playerx, y);
          }
        }
      }
    }
    //std::cout<<"Beginning pathfinding\n";
    findPathableSpace(playerx, playery);
    vectorposition = rand() % pathableTiles.size();
    tile = pathableTiles.at(vectorposition);
    tile = Tile('<', false, 1, 2, tile.getXpos(), tile.getYpos());
    tileGrid[tile.getXpos()][tile.getYpos()] = tile;
  }

}; //end of class brace

#endif